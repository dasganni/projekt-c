#!/bin/bash

###### Add User to the passwd, if the uid is not the default one ######
echo "Adding UID to temporary passwd"
export USER_ID=$(id -u)
envsubst < /etc/nginx/nss-wrapper/passwd.template > /etc/nginx/nss-wrapper/passwd
echo "Finished Adding UID to temporary passwd"

#wait for certificates to be available if no exists
while [ ! -f /etc/nginx/certificates/fullchain.pem ]
do
  echo "Waiting for certificate to be created. Checking every second."
  sleep 1 # or less like 0.2
done

### Check for changes in the certificate (i.e renewals or first start) and send this process to background
$(while inotifywait -e close_write /etc/nginx/certificates; do nginx -s reload; done) &

###### Start Nginx ######
echo "Starting Nginx"
nginx -g 'daemon off;'