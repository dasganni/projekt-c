map $http_upgrade $connection_upgrade {
    default upgrade;
    '' close;
}

upstream backend {
    server backend:8000;
}

upstream backend-socket {
    server backend-socket:8000;
}

upstream vuejs {
    server vuejs:8080;
}

server {

    listen 8081 default_server; #http, will be redirected

    # Redirect non-https traffic to https
    return 301 https://$host$request_uri;
}


server {

    listen 8080 ssl; #https
              
    location / {
      try_files $uri $uri/ /etc/nginx/html/index.html @vuejs-node;
    }

    location @vuejs-node {
        proxy_pass http://vuejs;
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto https;
        proxy_set_header X-Frame-Options SAMEORIGIN;
        proxy_pass_request_headers on;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_redirect off;
    }

    location /api/ {
        proxy_pass http://backend;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto https;
        proxy_set_header Host $http_host;
        proxy_pass_request_headers on;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_redirect off;
    }

    location /api-auth/ {
    proxy_pass http://backend;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto https;
    proxy_set_header Host $http_host;
    proxy_pass_request_headers on;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_redirect off;
}

    location /admin/ {
        proxy_pass http://backend;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto https;
        proxy_set_header Host $http_host;
        proxy_pass_request_headers on;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_redirect off;
    }

    location /ws/ {
        proxy_pass http://backend-socket/ws/;

        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto https;
        proxy_set_header X-Frame-Options SAMEORIGIN;
        proxy_pass_request_headers on;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_redirect off;
    }

    location /static/ {
        alias /etc/nginx/allstaticfiles/;
    }

    location /media/ {
        alias /etc/nginx/media/;
    }

    location /health {
        return 200 'alive';
        add_header Content-Type text/plain;
    }

    # letsencrypt acme challenge for domain verification
    location /.well-known/acme-challenge/ {
        alias /etc/nginx/certbot/.well-known/acme-challenge/;
    }

    ssl_certificate /etc/nginx/certificates/fullchain.pem;
    ssl_certificate_key /etc/nginx/certificates/privkey.pem;

}