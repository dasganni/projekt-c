@echo off
set Environment=prod
set /p Environment="Which server environment should be used? Press [ENTER] for default [%Environment%] (Choices: prod, dev): "
echo "%Environment% environment is used"
IF %Environment% == dev (
    cmd /k "docker-compose -f ".\docker-compose-dev.yml" down"
) ELSE (
    cmd /k "docker-compose -f ".\docker-compose-prod.yml" down"
)