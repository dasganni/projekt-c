#!/bin/bash

###### Add User to the passwd, if the uid is not the default one ######
echo "Adding UID to temporary passwd"
export USER_ID=$(id -u)
envsubst < /docker/passwd.template > /docker/passwd
echo "Finished Adding UID to temporary passwd"

###### Start normal entrypoint of redis ######
exec /usr/local/bin/docker-entrypoint.sh "$@"