#!/bin/bash

echo "Please check if all configs in the .env file are correct!"
read -p "Which server environment should be used? Press [ENTER] for default [prod] (Choices: prod, dev):" -e -i 'prod' environment
echo "$environment environment is used"
echo "Press Control+C to stop the server or run the stop-server.bat file"
if [ $environment = 'dev' ]
then
    docker-compose -f "docker-compose-dev.yml" down
else
    docker-compose -f "docker-compose-prod.yml" down
fi