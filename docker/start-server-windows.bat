@echo off
set Environment=prod
echo "Please check if all configs in the .env file are correct!"
set /p Environment="Which server environment should be used? Press [ENTER] for default [%Environment%] (Choices: prod, dev): "
echo "%Environment% environment is used"
echo "Press Control+C to stop the server or run the stop-server.bat file"
IF %Environment% == dev (
    cmd /k "docker-compose -f ".\docker-compose-dev.yml" up"
) ELSE (
    cmd /k "docker-compose -f ".\docker-compose-prod.yml" up"
)