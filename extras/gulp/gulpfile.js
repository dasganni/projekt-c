/*
# Do this in the directory above your project git dir
# Copy the gulp JS in that dir and move on with the next steps

# We will want to install this globally
npm install sass -g 

# Install Gulp CLI. This will make your life easier when you have the Gulp command line interface.
npm install gulp-cli -g

# Install Gulp and all required components.
npm install gulp gulp-sourcemaps browser-sync gulp-sass gulp-autoprefixer gulp-minify gulp-minify-css gulp-rename gulp-autoprefixer --save-dev

# Setting up the package.
npm init

        # For the file-watcher and browserSync use:
        gulp watch  

        # to compile Scss, minify and autoprefix use
        gulp style

        # to minify and compile the custom js file use
        gulp compress


*/

// gulp basics
const gulp = require('gulp');
const sass = require('gulp-sass');
// browserSync
const browserSync = require('browser-sync').create();
// create source-maps
const sourcemaps = require('gulp-sourcemaps');
// minfy the Css/JS
const minify = require('gulp-minify');
const minCss = require('gulp-minify-css');
// to rename the min file
const rename = require('gulp-rename');

// Auto prefixer with default settings
const autoprefixer = require('gulp-autoprefixer');
/* Right now, we run with the default configuration from Autoprefixer which is

Browsers with over 1% market share,
Last 2 versions of all browsers,
Firefox ESR,
Opera 12.1 */

// configs and directories
var config = {
    pathStatic: './ProjectC/app/static',
    srcCss : './css',
    srcScss : '/scss/**/*.scss',
    buildCss: '/dist/css',
    srcJS: '/js/custom/**/*.js',
    buildJS: '/dist/js',
    srcHtml: './ProjectC/app/**/*.html'
 }





//compile scss into css
function style() {
    return gulp.src(config.pathStatic + config.srcScss)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error',sass.logError))

    //.pipe(autoprefixer())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(config.pathStatic+config.buildCss))

    // output the minified version
    .pipe(minCss({ keepSpecialComments: 1, processImport: false }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest(config.pathStatic+config.buildCss))

    .pipe(browserSync.stream());
}

//start brwoserSync
function watch() {
    browserSync.init({
        ui: false,
        proxy: {
            target: "https://localhost:8000/",
            proxyRes: [
                function(proxyRes, req, res) {
                    console.log(proxyRes.headers);
                }
            ]
        }
    });

    gulp.watch(config.srcHtml).on('change', browserSync.reload);
    gulp.watch(config.pathStatic + config.srcScss, style)
    //gulp.watch('./*.html').on('change',browserSync.reload);
    gulp.watch(config.pathStatic + config.srcJS).on('change', compress);
    gulp.watch(config.pathStatic + config.srcJS).on('change', browserSync.reload);
}


//minify JS
function compress() {
    gulp.src([config.pathStatic + config.srcJS])
    .pipe(minify())
    .pipe(gulp.dest(config.pathStatic + config.buildJS))
    .pipe(browserSync.stream());
  }



// export the functions to Gulp_CLI
exports.watch = watch;
exports.style = style;
exports.compress = compress;