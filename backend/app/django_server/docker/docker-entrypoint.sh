#!/bin/bash

###### Add User to the passwd, if the uid is not the default one ######
echo "Adding UID to temporary passwd"
export USER_ID=$(id -u)
envsubst < /pnp-manager/django_server/docker/passwd.template > /pnp-manager/django_server/docker/passwd
echo "Finished Adding UID to temporary passwd"


###### Wait for postgres to start ######
function postgres_ready(){
python << END
import sys
import psycopg2
try:
    conn = psycopg2.connect(dbname="$DJANGO_DB_NAME", user="$DJANGO_DB_USER", password="$DJANGO_DB_PASSWORD", host="$DJANGO_DB_HOST")
except psycopg2.OperationalError:
    sys.exit(-1)
sys.exit(0)
END
}

# for development with mounted folder needed
if [ $DJANGO_SERVER_ENVIRONMENT = 'dev' ]
  then
    mkdir /pnp-manager/django_server/configs/
    mkdir /pnp-manager/allstaticfiles/
    mkdir -p /pnp-manager/django_server/certbot/.well-known/acme-challenge/
    mkdir /pnp-manager/django_server/certificates/
    mkdir -p /pnp-manager/media/{maps}
    mkdir /pnp-manager/django_server/letsencrypt/
fi


until postgres_ready; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up - continuing..."

# create settings.cfg file if it does not exist
if [ ! -f "/pnp-manager/django_server/configs/settings.cfg" ] 
then
  echo "settings.cfg file does not exist, creating it now!"
  touch /pnp-manager/django_server/configs/settings.cfg
  chmod g+w /pnp-manager/django_server/configs/settings.cfg
else
  echo "settings.cfg file exists, using it"
fi

# create secret_key file if it does not exist
if [ ! -f "/pnp-manager/django_server/configs/secret_key" ] 
then
  echo "secret_key file does not exist, creating it now!"
  touch /pnp-manager/django_server/configs/secret_key
  chmod g+w /pnp-manager/django_server/configs/secret_key
else
  echo "secret_key file exists, using it"
fi

# create a self-signed certificate if no certificate exists
if [ ! -f "/pnp-manager/django_server/certificates/fullchain.pem" ] 
then
  echo "No certificates found, generating self-signed certificate with private key under: /pnp-manager/django_server/certificates/"
  openssl req -newkey rsa:4096 \
          -x509 \
          -sha256 \
          -days 3650 \
          -nodes \
          -out /pnp-manager/django_server/certificates/fullchain.pem \
          -keyout /pnp-manager/django_server/certificates/privkey.pem \
          -subj "/CN=localhost"
else
  echo "Certificate found, using it!"
fi

if [ $DJANGO_SERVER_TYPE = 'daphne' ]
then
  ###### Start Daphne server ######
  echo "This is a Daphne Server"

  # create daphne-access.log file if it does not exist
  if [ ! -f "/pnp-manager/daphne-access.log" ] 
  then
    echo "daphne-access.log file does not exist, creating it now!"
    touch /pnp-manager/daphne-access.log
    chmod g+w /pnp-manager/daphne-access.log
  else
    echo "daphne-access.log file exists, using it"
  fi

  daphne -b 0.0.0.0 -p 8000 --access-log /pnp-manager/daphne-access.log django_server.asgi:application #start server with config file and write master-pid to a file for restart
else
  ###### Start Gunicorn server ######
  echo "This is a Gunicorn Server"

  ###### Collect static files ######
  echo "Collect static files"
  python manage.py collectstatic --noinput

  ###### Apply database migrations ######
  echo "Apply database migrations"
  python manage.py makemigrations
  python manage.py migrate

  ###### Create superuser ######
  echo "Creating initial Superuser with Username: $DJANGO_SUPERUSER_ADMIN and Email $DJANGO_SUPERUSER_EMAIL"
  python manage.py createsuperuser --noinput --username $DJANGO_SUPERUSER_ADMIN --email $DJANGO_SUPERUSER_EMAIL

  if [ $DJANGO_SERVER_ENVIRONMENT = 'dev' ]
  then
    echo "Starting development server"
    gunicorn django_server.wsgi:application --reload --config /pnp-manager/django_server/gunicorn.conf.py -p /tmp/gunicorn.pid #start server with config file and write master-pid to a file for restart
  else
    echo "Starting production server"
    gunicorn django_server.wsgi:application --config /pnp-manager/django_server/gunicorn.conf.py -p /tmp/gunicorn.pid #start server with config file and write master-pid to a file for restart
  fi
fi