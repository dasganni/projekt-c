# file gunicorn.conf.py
# coding=utf-8
# Reference: https://github.com/benoitc/gunicorn/blob/master/examples/example_config.py
import os
import multiprocessing

loglevel = 'info'

bind = '0.0.0.0:8000'
workers = os.getenv('GUNICORN_WORKERS', multiprocessing.cpu_count() * 2 + 1)
threads = os.getenv('GUNICORN_THREADS', 2)

# certfile = '/pnp-manager/django_server/certificates/fullchain.pem'
# keyfile = '/pnp-manager/django_server/certificates/privkey.pem'

timeout = 3 * 60  # 3 minutes
keepalive = 24 * 60 * 60  # 1 day

capture_output = True