#!/bin/bash

#sends a specific signal which lets gunicorn restart its service and reload all files
kill -s HUP $(cat /tmp/gunicorn.pid)