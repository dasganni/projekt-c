import configparser
import os
import logging

logger = logging.getLogger(__name__)

logger.info("Using productive environment!")
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# Access configparser to load variable values
config = configparser.ConfigParser(allow_no_value=True)
config.read([os.path.join(BASE_DIR, "django_server/default.cfg"),
             os.path.join(BASE_DIR, "django_server/configs/settings.cfg")])


###########PROD SETTINGS##############


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

WSGI_APPLICATION = "django_server.wsgi.application"
