"""django_server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import include, path
from rest_framework.routers import DefaultRouter

from game.api.views import GameViewSet
from maptool.api.views import MaptoolViewSet
# from usermanager.api.views import UsermanagerViewSet
from djoser import views as djoserviews

# Create a router and register the viewsets with it.
router = DefaultRouter()
router.register('games', GameViewSet)
router.register('maptool', MaptoolViewSet)
# router.register('usermanager', UsermanagerViewSet)
router.register("users", djoserviews.UserViewSet)

# Register your models here.
admin.site.login = login_required(admin.site.login)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('user-management/', include("usermanager.urls",
                                     namespace="usermanager")),
    path('accounts/', include('allauth.urls')),
    path('dice/', include('dice.urls')),
    path('character/', include('character.urls')),
    path('settings/', include('settings.urls')),
    path('game/', include('game.urls')),
    path('chattool/', include('chattool.urls')),
    path('', include('index.urls')),

    path('api/', include(router.urls), name="api-root"),
    path('api-auth/', include("rest_framework.urls"))
]

if bool(settings.DEBUG):

    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    # only in debug=true - For production nginx or
    # apache must serve the media files
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
    # only in debug=true - For production nginx or
    # apache must serve the media files
    urlpatterns += staticfiles_urlpatterns()

handler500 = 'servererrors.views.internalServerErrorView'
