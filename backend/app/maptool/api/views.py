from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from maptool.api.serializers import MapSerializer
from maptool.models import Map


class MaptoolViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """
    queryset = Map.objects.all()
    serializer_class = MapSerializer
    permission_classes = [IsAuthenticated]
