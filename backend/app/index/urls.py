from django.urls import path, include
from . import views


app_name = 'index'

urlpatterns = [
    path('', views.indexView.as_view(), name='index'),
    path("chattool/", include('chattool.urls')),
]
