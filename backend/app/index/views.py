from django.views.generic import TemplateView


class indexView(TemplateView):
    template_name = 'index/index.html'

    def get_context_data(self, **kwargs):

        context = super(indexView, self).get_context_data(**kwargs)
        context["app_type"] = "index"
        context["room_name"] = "globalchat"
        return context
