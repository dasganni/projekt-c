from rest_framework import serializers
from usermanager.models import User


class UserSerializer(serializers.ModelSerializer):

    last_login = serializers.ReadOnlyField()
    is_superuser = serializers.ReadOnlyField()
    is_staff = serializers.ReadOnlyField()
    is_active = serializers.ReadOnlyField()
    date_joined = serializers.ReadOnlyField()
    username_slug = serializers.ReadOnlyField()

    class Meta:
        model = User
        fields = "__all__"
