import configparser
import os
import subprocess
from time import sleep

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy

from django.views.generic.edit import FormView

#from settings.models import *

from . import forms




# Create your views here.
class settingsView(LoginRequiredMixin, FormView):
    template_name = "settings/settings.html"
    form_class = forms.settingsForm
    success_url = reverse_lazy("settings:settings")

    def form_valid(self, form):
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        # Access configparser to load variable values from cfg file
        config = configparser.ConfigParser(allow_no_value=True)

        config.read([os.path.join(BASE_DIR, "django_server/default.cfg"), os.path.join(BASE_DIR, "django_server/configs/settings.cfg")])


        config.set("default", "publichostname", form.cleaned_data["publicHost"])

        # user-registration-config
        if form.cleaned_data["enableNewUserRegistration"]:
            enableNewUserRegistration = "yes"
        else:
            enableNewUserRegistration = "no"
        config.set(
            "authentication", "enableNewUserRegistration", enableNewUserRegistration
        )

        # facebook-config
        if form.cleaned_data["usefacebookauth"]:
            usefacebookauth = "yes"
        else:
            usefacebookauth = "no"
        config.set("authentication", "usefacebookauth", usefacebookauth)
        config.set(
            "authentication", "facebookappid", form.cleaned_data["facebookappid"]
        )
        config.set(
            "authentication", "facebookappkey", form.cleaned_data["facebookappkey"]
        )

        # google-config
        if form.cleaned_data["useGoogleAuth"]:
            useGoogleAuth = "yes"
        else:
            useGoogleAuth = "no"
        config.set("authentication", "useGoogleAuth", useGoogleAuth)
        config.set(
            "authentication", "googleClientID", form.cleaned_data["googleClientID"]
        )
        config.set(
            "authentication",
            "googleClientSecret",
            form.cleaned_data["googleClientSecret"],
        )

        #mailsending-config
        if form.cleaned_data["emailUseTLS"]:
            emailUseTLS = "yes"
        else:
            emailUseTLS = "no"

        if form.cleaned_data["useMailSending"]:
            usemailSending = "yes"
        else:
            usemailSending = "no"


        config.set("mailserver", "usemailSending", usemailSending)
        config.set("mailserver", "emailUseTLS", emailUseTLS)

        config.set("mailserver", "emailHost", form.cleaned_data["emailHost"])
        config.set("mailserver", "emailPort", str(form.cleaned_data["emailPort"]))
        config.set("mailserver", "emailHostUser", form.cleaned_data["emailHostUser"])
        config.set("mailserver", "emailHostUserPassword", form.cleaned_data["emailHostUserPassword"])
        config.set("mailserver", "defaultFromEmail", form.cleaned_data["defaultFromEmail"])

        # write config into config-file
        with open(
            os.path.join(BASE_DIR, "django_server/configs/settings.cfg"), "w"
        ) as configfile:
            config.write(configfile)



        #restart the webserver
        subprocess.call(os.path.join(BASE_DIR, "django_server/gunicorn_restart.sh"))

        #wait for the write to finish before reloading the server
        sleep(1.0)

        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(settingsView, self).get_context_data(**kwargs)
        context["allowedHosts"] = settings.ALLOWED_HOSTS
        return context


class certbotView(LoginRequiredMixin, FormView):
    template_name = "settings/certbot.html"
    form_class = forms.certbotForm
    success_url = reverse_lazy("settings:certbot")

    def get_context_data(self, **kwargs):
        context = super(certbotView, self).get_context_data(**kwargs)
        return context



    def form_valid(self, form):
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        # Access configparser to load variable values from cfg file
        config = configparser.ConfigParser(allow_no_value=True)

        config.read([os.path.join(BASE_DIR, "django_server/default.cfg"), os.path.join(BASE_DIR, "django_server/configs/settings.cfg")])
    
        config.set("certbot", "requestmail", form.cleaned_data["requestmail"])
        config.set("certbot", "certdomain", form.cleaned_data["certdomain"])


        # write config into config-file
        with open(
            os.path.join(BASE_DIR, "django_server/configs/settings.cfg"), "w"
        ) as configfile:
            config.write(configfile)



        #request certificate from letsencrypt
        subprocess.call(['certbot', 'certonly', '--webroot', '--non-interactive', '--agree-tos', \
            '--webroot-path', os.path.join(BASE_DIR, "django_server/certbot/"), \
            '--domain', form.cleaned_data["certdomain"], \
            '-m', form.cleaned_data["requestmail"], \
            '--key-path', os.path.join(BASE_DIR, "django_server/certificates/privkey.pem"), \
            '--cert-path', os.path.join(BASE_DIR, "django_server/certificates/fullchain.pem"), \
            '--work-dir', os.path.join(BASE_DIR, "django_server/letsencrypt/"), \
            '--logs-dir', os.path.join(BASE_DIR, "django_server/letsencrypt/"), \
            '--config-dir', os.path.join(BASE_DIR, "django_server/letsencrypt/")
        ])

        #copy the certificate into the volume
        subprocess.call(['cp', '-rL', os.path.join(BASE_DIR, "django_server/letsencrypt/live/" + form.cleaned_data["certdomain"] + "/."), \
            os.path.join(BASE_DIR, "django_server/certificates/")
        ])

        sleep(1.0)
        # restart the webserver
        # subprocess.call(os.path.join(BASE_DIR, "django_server/gunicorn_restart.sh"))

        return super().form_valid(form)
