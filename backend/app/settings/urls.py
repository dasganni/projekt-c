from django.urls import path
from django.conf.urls import url
from . import views


app_name = 'settings'

urlpatterns = [
    url('server-settings/', views.settingsView.as_view(), name='settings'),
    url('certbot/', views.certbotView.as_view(), name='certbot'),
]
