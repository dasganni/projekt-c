from django.apps import AppConfig


class ChattoolConfig(AppConfig):
    name = 'chattool'
