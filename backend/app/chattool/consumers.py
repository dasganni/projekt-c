import json

from channels.generic.websocket import AsyncWebsocketConsumer
from channels.db import database_sync_to_async

import logging

from game.models import Game

logger = logging.getLogger(__name__)


class ChatConsumer(AsyncWebsocketConsumer):

    @database_sync_to_async
    def get_game_object(self, game_id):
        game = Game.objects.get(id=game_id)
        return game

    @database_sync_to_async
    def has_user_perm(self, user, modelobject, permission):
        allowed = user.has_perm(permission, modelobject)
        return allowed

    async def connect(self):
        user = self.scope["user"]
        access_allowed = False
        room_name = self.scope['url_route']['kwargs']['room_name']

        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name

        if user.is_anonymous:
            # Reject the connection
            logger.warning("Not logged in user tried to connect to websocket")
            await self.close()
        else:
            # Game Chat Permissions Check
            if self.scope['url_route']['kwargs']['app_type'] == "game":
                tmp = room_name.split('_')
                game_id = tmp[1]
                logger.debug(user.username +
                             " tries to connect to the game-chat of game " + game_id)
                game = self.get_game_object(game_id)

                if self.has_user_perm(user, game, "game.view_game"):
                    logger.debug(
                        user.username +
                        " has the rights to connect to the game-chat of game "
                        + game_id
                    )
                    access_allowed = True
            elif self.scope['url_route']['kwargs']['app_type'] == "index":
                logger.debug(user.username +
                             " tries to connect to the globalchat!")
                access_allowed = True

            if access_allowed:

                await self.channel_layer.group_add(
                    self.room_group_name,
                    self.channel_name
                )

                await self.accept()
            else:
                await self.close()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message
        }))
