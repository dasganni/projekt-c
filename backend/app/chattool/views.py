from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView

# Tutorial
# Create your views here.


class ChattoolView(LoginRequiredMixin, TemplateView):
    template_name = "chattool/chattool.html"


class ChatRoomView(LoginRequiredMixin, TemplateView):
    template_name = "chattool/rooms.html"

    def get_context_data(self, **kwargs):
        context = super(ChatRoomView, self).get_context_data(**kwargs)
        context["room_name"] = self.kwargs["room_name"]
        return context
