from django.urls import path

from . import consumers

websocket_urlpatterns = [
    path(r'ws/chattool/<str:app_type>/<str:room_name>/',
         consumers.ChatConsumer),
]
