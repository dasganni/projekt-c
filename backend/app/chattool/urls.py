from django.urls import path
from . import views

app_name = 'chattool'

urlpatterns = [
    path("", views.ChattoolView.as_view(), name="chattool"),
    path("<str:room_name>/", views.ChatRoomView.as_view(), name="rooms"),

]
