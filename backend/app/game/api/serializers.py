from rest_framework import serializers
from game.models import Game
from usermanager.models import User


class PlayerCreatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username']


class ListRetrieveGameSerializer(serializers.ModelSerializer):

    creator = PlayerCreatorSerializer()
    allowed_players = PlayerCreatorSerializer(many=True)

    class Meta:
        model = Game
        fields = "__all__"


class GameSerializer(serializers.ModelSerializer):

    creator = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Game
        fields = "__all__"
