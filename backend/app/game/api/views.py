from rest_framework import permissions, viewsets, pagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from game.api.serializers import GameSerializer, ListRetrieveGameSerializer
from game.models import Game


class GameViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """
    queryset = Game.objects.all()
    serializer_class = GameSerializer
    pagination_class = pagination.PageNumberPagination
    page_size = 100
    permission_classes = [IsAuthenticated, permissions.DjangoObjectPermissions]
    perms_map = {
        'GET': ['%(app_label)s.view_%(model_name)s'],
        'OPTIONS': ['%(app_label)s.view_%(model_name)s'],
        'HEAD': ['%(app_label)s.view_%(model_name)s'],
        'POST': ['%(app_label)s.add_%(model_name)s'],
        'PUT': ['%(app_label)s.change_%(model_name)s'],
        'PATCH': ['%(app_label)s.change_%(model_name)s'],
        'DELETE': ['%(app_label)s.delete_%(model_name)s'],
    }

    def list(self, request):
        queryset = Game.objects.all()
        serializer = ListRetrieveGameSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = ListRetrieveGameSerializer(instance)
        return Response(serializer.data)

    # def assign_perms():
    #     assign_perm("view_game", self.request.user, self.object)
    #     assign_perm("change_game", self.request.user, self.object)
    #     assign_perm("delete_game", self.request.user, self.object)
    #     if self.object.access_restricted:
    #         for allowed_player in form.cleaned_data.get('allowed_players'):
    #             assign_perm("view_game", allowed_player, self.object)
    #     else:
    #         default_usergroup = Group.objects.get(name="default_usergroup")
    #         assign_perm("view_game", default_usergroup, self.object)
    #     return super(createGame, self).form_valid(form)
