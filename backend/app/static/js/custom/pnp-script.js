
jQuery(document).ready(function ($) {

    $('#button-sidebar-left').click(function () {
        if ($('#sidebar-left').hasClass('open')) {
            $("#sidebar-left").removeClass("open").addClass("closed");
            $("#button-sidebar-left").removeClass("open").addClass("closed");

        } else {
            $("#sidebar-left").addClass("open").removeClass("closed");
            $("#button-sidebar-left").addClass("open").removeClass("closed");
        }
    })

});