#!/bin/bash

###### Add User to the passwd, if the uid is not the default one ######
echo "Adding UID to temporary passwd"
export USER_ID=$(id -u)
envsubst < /docker/passwd.template > /docker/passwd
echo "Finished Adding UID to temporary passwd"

# for development with mounted folder needed
if [ $NODE_ENV = 'development' ]; then
    if [ ! -d "/pnp-manager-frontend/node-modules" ]; then
        npm install @vue/cli && npm install
    fi
    #Start Development Server
    echo "Starting development server"
    npm run serve
else
    echo "Productive server by creating files!"
    npm run build
    if [ "$(ls -A /pnp-manager-frontend/dist-volume/)" ]; then
        echo "Clearing old Dist-Files"
        rm -r /pnp-manager-frontend/dist-volume/*
    fi
    cp -r /pnp-manager-frontend/dist/. /pnp-manager-frontend/dist-volume/
fi